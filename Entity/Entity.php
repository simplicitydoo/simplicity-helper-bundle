<?php

namespace Simplicity\HelperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity
 *
 */
abstract class Entity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    protected $updated;

    /**
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @param  \DateTime $created
     * @return Entity
     */
    public function setCreated(\DateTime $created) {
        $this->created = $created;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @param  \DateTime $updated
     * @return Entity
     */
    public function setUpdated(\DateTime $updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     */
    public function prePersist()
    {
      $this->created = new \DateTime();
      $this->updated = new \DateTime();
    }
    
    /**
     */
    public function preUpdate()
    {
      $this->updated = new \DateTime();
    }
    
}
