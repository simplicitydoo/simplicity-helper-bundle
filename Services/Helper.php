<?php

namespace Simplicity\HelperBundle\Services;


class Helper {
  
  private $httpClient;
  
  function __construct($httpClient) {
    $this->httpClient = $httpClient;
  }
  
  public function getRandomString($length = 10) {
    $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $string = "";

    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters)-1)];
    }

    return $string;
  }
  
  public function geoLimit($remoteAddres) {
    $redirect = 0;
    if (!preg_match('~\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}~', $remoteAddres) || $this->httpClient->fetch_url('http://api.wipmania.com/' . $remoteAddres . '?k=J3k-v1AEDwRqviGNdKAqNpaZiA1') != 'RS') {
      if ($this->httpClient->get_http_response_code() == 200) {
        $redirect = 1;
      }
    }
    return $redirect;
  }
    
}