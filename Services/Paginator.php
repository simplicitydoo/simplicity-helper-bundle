<?php

namespace Simplicity\HelperBundle\Services;

class Paginator {
  function __construct() {
    
  }
  
  public function paginate($em, $getFunc, $getFuncArgs, $page, $itemVarName, $paginationPath, $countFnc = 'getCount', $countFncArgs = array(), $itemsPerPage = 25) {
    if (!$page) {
      $limit = 0; // limit for select query
      $page  = 1; // current page
    } else {
      $limit = ($page - 1) * $itemsPerPage;
    }
    
    $getFuncArgs = array_merge(array($limit, $itemsPerPage), $getFuncArgs);
    $items = call_user_func_array(array($em, $getFunc), $getFuncArgs);
    
    $totalCount = call_user_func_array(array($em, $countFnc), $countFncArgs);
    error_log($totalCount);
    
    $tplArgs = array(
                    'currentFilters' => array(),
                    'currentPage' => $page,
                    'paginationPath' => $paginationPath,
                    'showAlwaysFirstAndLast' => true,
                    'lastPage' => ceil($totalCount/$itemsPerPage),
                    'totalCount' => $totalCount,
                    'limit' => $limit,
                    'itemsPerPage' => $itemsPerPage,
                    $itemVarName => $items
                    );
    
    return $tplArgs;
  }
  
}
